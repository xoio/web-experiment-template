
module.exports = {
    entry: [],
    output:{
        path:__dirname + "/public/js",
        filename:"app.js"
    },
    module:{
        loaders:[
            {
                test:/\.js$/,
                exclude:/(node_modules|bower_components)/,
                loader:'babel-loader',
                query:{
                    presets:['babel-preset-es2015']
                }
            },
            {
                test:/\.js$/,
                exclude:/(node_modules|bower_components)/,
                loader:`kantan/lib/devsetup.js`,
                query:{
                    css:"../css/main.css"
                }
            },
            { test: /\.(glsl|frag|vert)$/, loader: 'raw', exclude: /node_modules/ },
            { test: /\.(glsl|frag|vert)$/, loader: 'glslify', exclude: /node_modules/ }
        ]
    },
    plugins:[]
};