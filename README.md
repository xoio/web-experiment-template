👋

web-experiment-template
=====

This provides a base for starting a web experiment. 


It utilizes ES6 throughout as much as possible and provides a Webpack based setup that includes hot reload of code. Theres some boilerplate to use as a starting point.

How to use
====
* clone
* npm install
* `npm run server`
* You can open up a browser and go to `http://localhost:9000`